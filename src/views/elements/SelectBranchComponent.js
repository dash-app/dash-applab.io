import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import server from '../../gitlab/server';

const { AsyncTypeahead } = require("react-bootstrap-typeahead");

export default class SelectBranchComponent extends Component {

    constructor() {
        super();
        this.state = {
            options: []
        };
        this.loading = false;
        this.handleSearch = this.handleSearch.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    async handleSearch(query) {
        // getting all branches for the project
        if (this.loading) return;
        this.loading = true;
        let projects = await server.getProjectBranches(this.props.projectId, true);
        projects = projects.map(p => {
            return p.name;
        });
        this.setState({
            options: projects
        });
        this.loading = false;
    }

    onChange(event) {
        // we got the selected value
        if (event.length === 0) {
            this.props.onChange('');
            return;
        }
        if (this.props.onChange) {
            this.props.onChange(event[0].branch || event[0]);
        }
        if (event && event.length) {
            this.element.blur();
        }
    }

    render() {
        return (
            <AsyncTypeahead
                id={this.props.id}
                isLoading={false}
                labelKey="branch"
                ref={x => this.element = x}
                minLength={0}
                allowNew={this.props.newText ? true : false}
                newSelectionPrefix={this.props.newText || ""}
                onSearch={this.handleSearch}
                options={this.state.options}
                placeholder="Type..."
                onFocus={this.handleSearch}
                renderMenuItemChildren={(option, props) => (
                    <Fragment>
                        <span>{option}</span>
                    </Fragment>
                )}
                onChange={this.onChange}
            />
        );
    }

}

SelectBranchComponent.propTypes = {
    projectId: PropTypes.number.isRequired,
    id: PropTypes.string.isRequired,
    newText: PropTypes.string
};