import React, { Component } from 'react';
import {
  Button,
  Col,
  Card,
  CardFooter,
  CardBody, Modal, ModalHeader, ModalBody, ModalFooter,
  CardTitle,
  Row,
  Label,
  Input,
  Breadcrumb,
  BreadcrumbItem
} from 'reactstrap';
import { Avatar } from '../../vibe';
import { Loader } from '../../vibe/';
import { Button as MButton } from '@material-ui/core';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import server from '../../gitlab/server';
// import laptopImage from '../../assets/images/laptop.jpeg';
// import avatar2 from '../../assets/images/avatar2.jpeg';

class ProjectCard extends Component {
  constructor(props) {
    super(props);
    let project = props.project;
    // project.tags = [];
    project.namespace = project.namespace || "";
    project.selected = project.selected || false;
    project.merge_requests = [
      {
        "id": 6450,
        "iid": 10,
        "project_id": 146,
        "title": "fixed imports",
        "description": "",
        "state": "merged",
        "created_at": "2020-03-30T11:25:09.281Z",
        "updated_at": "2020-03-30T11:25:13.310Z",
        "author": {
          "id": 28,
          "name": "Naveen Negi",
          "username": "naveen.negi",
          "state": "active",
          "avatar_url": "https://motorcode.concirrusquest.com/uploads/-/system/user/avatar/28/avatar.png",
          "web_url": "https://motorcode.concirrusquest.com/naveen.negi"
        },
        "merged_by": {
          "id": 28,
          "name": "Naveen Negi",
          "username": "naveen.negi",
          "state": "active",
          "avatar_url": "https://motorcode.concirrusquest.com/uploads/-/system/user/avatar/28/avatar.png",
          "web_url": "https://motorcode.concirrusquest.com/naveen.negi"
        },
        "merged_at": "2020-03-30T11:25:13.340Z",
        "target_branch": "develop",
        "source_branch": "QM-1837",
        "sha": "c40419f81a94e0896538e04bc232472b3a1490d7",
        "merge_commit_sha": "b41f17c1871a12f7cdb1d3cb7f9be2f4969ebf20",
        "web_url": "https://motorcode.concirrusquest.com/questmotor/backend/services/admin-app/merge_requests/1",
      },
      {
        "id": 6449,
        "iid": 9,
        "project_id": 146,
        "title": "Develop",
        "description": "",
        "state": "merged",
        "created_at": "2020-03-30T11:23:48.233Z",
        "updated_at": "2020-03-30T11:27:14.052Z",
        "author": {
          "id": 28,
          "name": "Naveen Negi",
          "username": "naveen.negi",
          "state": "active",
          "avatar_url": "https://motorcode.concirrusquest.com/uploads/-/system/user/avatar/28/avatar.png",
          "web_url": "https://motorcode.concirrusquest.com/naveen.negi"
        },
        "merged_by": {
          "id": 39,
          "name": "Rajdeep",
          "username": "rajdeep.deb",
          "state": "active",
          "avatar_url": "https://motorcode.concirrusquest.com/uploads/-/system/user/avatar/39/avatar.png",
          "web_url": "https://motorcode.concirrusquest.com/rajdeep.deb"
        },
        "merged_at": "2020-03-30T11:27:14.084Z",
        "target_branch": "RC-v2.6.0",
        "source_branch": "develop",
        "sha": "a01f0d091bd2fa01a5a2f8be76e210d6f4cc6b01",
        "merge_commit_sha": "ac1393755fa20dca703f7627271405ca1021b39c",
        "web_url": "https://motorcode.concirrusquest.com/questmotor/backend/services/admin-app/merge_requests/9",
      }
    ];
    this.parentUpdate = props.parentUpdate;
    this.toggleModal = this.toggleModal.bind(this);
    this.toggleLoading = this.toggleLoading.bind(this);
    this.toggleSelected = this.toggleSelected.bind(this);
    this.getTags = this.getTags.bind(this);
    this.state = {
      loading: true,
      modal: false,
      project: project
    };
    this.state.project.merge_requests = [];
  }

  toggleSelected() {
    let project = this.state.project;
    project.selected = !project.selected;
    this.setState(prevState => ({
      project: project
    }), () => {
      this.parentUpdate(project);
    });
  }

  toggleModal() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  toggleLoading() {
    this.setState(prevState => ({
      loading: !prevState.loading
    }));
  }

  async getTags(cache = true) {
    this.toggleLoading();
    let tags = await server.getProjectTags(this.state.project.id, cache);
    let p = {...this.state.project};
    if(tags && tags.length > 0) {
      tags = [{
        name: tags[0].name,
        web_url: `${this.state.project.web_url}/commit/${tags[0].target}`
      }];
    } else {
      tags = [{
        name: 'No Tags',
        web_url: undefined
      }]
    }
    this.state.project.tags = tags;
    p.tags = tags;
    this.setState(prevState => ({
      project: p,
      loading: false
    }));
    this.parentUpdate(p);
  }

  componentDidMount() {
    if(!this.state.project.tags) {
      setTimeout(() => {
        this.getTags();
      }, 10);
    } else {
      this.toggleLoading();
    }
  }

  render() {
    let tags = this.state.project.tags || [];
    return (
      <Col md={4}>
        <Card>
          <CardBody style={{padding: 20, minHeight: 200}}>
            {this.state.project.selected ?
              <CheckBoxIcon onClick={this.toggleSelected} className="pull-right text-info cursor-pointer" style={{fontSize: 30}}/>
              :
              <CheckBoxOutlineBlankIcon onClick={this.toggleSelected} className="pull-right text-info cursor-pointer" style={{fontSize: 30}}/>
            }
            <CardTitle style={{marginBottom: 30}}>
                <b onClick={this.toggleSelected}>{this.state.project.name}</b>
                <Breadcrumb>
                  {this.state.project.namespace.full_path.split("/").map(group => {
                    return <BreadcrumbItem key={group}>{group}</BreadcrumbItem>
                  })}
                </Breadcrumb>
            </CardTitle>
            {this.state.project.description && this.state.project.description.length > 0 ? this.state.project.description : "A description is missing for the project"}
          </CardBody>
          <CardFooter>
            {this.state.loading ?
              <div className="pull-right m-1">
                <Loader type="spin" small />
              </div> :
              <Button size="sm" className="pull-right" onClick={() => this.getTags(false)}>
                <i className="fa fa-refresh"></i>
              </Button>
            }
            {
              tags.map(tag => {
                return (<MButton key={tag.name} startIcon={<LocalOfferIcon />} href={tag.web_url} target="_blank"
                 variant="contained" color="inherit" className="btn-primary btn text-white text-transform-none"><b>{tag.name}</b></MButton>)
              })
            }
            {/* <Button href="https://google.com" target="_blank" variant="outlined" color="primary">
              <img src={<LocalOfferIcon />} /><b>v2.11.0</b>
            </Button> */}
            {/* list MRs */}
            {this.state.project.merge_requests.map((mr, index) => {
              return <span key={index}>
                <Button color="success" style={{ marginRight: 10 }} onClick={this.toggleModal}>{mr.title}</Button>
                <Modal isOpen={this.state.modal} toggle={this.toggleModal}>
                  <ModalHeader toggle={this.toggleModal}>
                    <h3>
                      <Button color="success" style={{ marginRight: 10 }}><i className="fa fa-check"></i>&nbsp;Merged</Button>
                      {mr.source_branch} -&gt; {mr.target_branch}
                    </h3>
                  </ModalHeader>
                  <ModalBody>
                    <Row className="m-b">
                      <Col>
                        <strong>Author</strong>
                        <div><Avatar image={mr.author.avatar_url} size="md" /> {mr.author.name}</div>
                      </Col>
                      <Col>
                        <strong>Merged By</strong>
                        <div><Avatar image={mr.merged_by.avatar_url} size="md" /> {mr.merged_by.name}</div>
                      </Col>
                    </Row>
                    {mr.description && mr.description.length > 0 ? mr.description : "A description is missing for the MR"}
                  </ModalBody>
                  <ModalFooter>
                    <a href={this.state.project.web_url + "/commit/" + mr.sha} rel="noopener noreferrer" target="_blank"><Button color="primary">View Commit&nbsp;&nbsp;<i className="fa fa-arrow-right"></i></Button>{' '}</a>
                    <a href={this.state.project.web_url + "/commit/" + mr.merge_commit_sha} rel="noopener noreferrer" target="_blank"><Button color="primary">Merge Commit&nbsp;&nbsp;<i className="fa fa-arrow-right"></i></Button>{' '}</a>
                    <a href={mr.web_url} rel="noopener noreferrer" target="_blank"><Button color="primary">View MR&nbsp;&nbsp;<i className="fa fa-arrow-right"></i></Button>{' '}</a>
                    {/* <Button color="secondary" onClick={this.toggleModal}>Cancel</Button> */}
                  </ModalFooter>
                </Modal>
              </span>
            })}
          </CardFooter>
        </Card>
      </Col>
    );
  }
}

export default ProjectCard;
