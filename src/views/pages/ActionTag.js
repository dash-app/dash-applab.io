import React, { Component } from 'react';
import { Row, Col, Card } from 'reactstrap';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
// import Stepper from 'react-stepper-horizontal';

import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import { StepConnector } from '@material-ui/core';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Check from '@material-ui/icons/Check';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { Loader } from '../../vibe';

import server from '../../gitlab/server';

let statuses = ["exists", "tag", "cancel"];
let stepperSteps = [
	{ title: 'Branches Exists', type: 'exists' },
	{ title: 'Push Tag', type: 'tag' },
	// { title: 'Merge MR', type: 'merge' },
	{ title: 'Cancel Pipeline', type: 'cancel' }
];

const QontoConnector = withStyles({
	alternativeLabel: {
		top: 10,
		left: 'calc(-50% + 16px)',
		right: 'calc(50% + 16px)',
	},
	active: {
		'& $line': {
			borderColor: '#0897ee',
		},
	},
	completed: {
		'& $line': {
			borderColor: '#0897ee',
		},
	},
	line: {
		borderColor: '#eaeaf0',
		borderTopWidth: 3,
		borderRadius: 1,
	},
})(StepConnector);

const useQontoStepIconStyles = makeStyles({
	root: {
		color: '#eaeaf0',
		display: 'flex',
		height: 22,
		alignItems: 'center',
	},
	active: {
		color: '#04ab3a',
	},
	circle: {
		width: 8,
		height: 8,
		borderRadius: '50%',
		backgroundColor: 'currentColor',
	},
	completed: {
		color: '#04ab3a',
		zIndex: 1,
		fontSize: 30
	},
});

function QontoStepIcon(props) {
	const classes = useQontoStepIconStyles();
	const { active, completed } = props;

	return (
		<div
			className={clsx(classes.root, {
				[classes.active]: active,
			})}
		>
			{/* {completed ? <Check className={classes.completed} /> : <div className={classes.circle} />} */}
			{completed ? <Check className={classes.completed} /> :
				active ? <div><Loader type="spin" small /></div> : <div className={classes.circle} />}
		</div>
	);
}

QontoStepIcon.propTypes = {
	/**
	 * Whether this step is active.
	 */
	active: PropTypes.bool,
	/**
	 * Mark the step as completed. Is passed to child components.
	 */
	completed: PropTypes.bool,
};

class TagProject extends Component {
	constructor(props) {
		super();
		this.project = props.project;
		this.tag_push = props.tag_push;
		this.target_branch = props.target_branch;
		this.cancel_pipeline = props.cancel_pipeline;
		this.parentUpdate = props.update;
		this.initNextStep = this.initNextStep.bind(this);
		this.state = {
			status: this.project.status,
			error: this.project.error
		}
	}

	componentDidMount() {
		setImmediate(this.initNextStep);
	}

	async initNextStep() {
		switch (this.project.status) {
			case 'exists':
				// create branch
				let tag = await server.pushtag(this.project.id, this.target_branch, this.tag_push);
				if (tag) {
					console.log(this.project.name, tag);
					this.project.tag = tag;
					this.project.status = 'tag';
					this.parentUpdate('tag');
					this.setState({
						status: 'tag'
					});
					setTimeout(() => {
						// this.initNextStep();
					}, 1000);
					setImmediate(this.initNextStep);
				} else {
					this.setState({
						error: 'tag'
					});
				}
				break;
			case 'tag':
				// cancel pipeline
				if(this.cancel_pipeline) {
					let res = await server.cancelPipelinesByRef(this.project.id, this.tag_push);
					console.log(this.project.name, res);
					if(!res) {
						this.setState({
							error: 'cancel'
						});
					}
				}
				// success
				this.project.status = 'cancel';
				this.parentUpdate('cancel');
				this.setState({
					status: 'cancel'
				});
				break;
			case 'cancel':
				break;
			default:
				// check branches
				let tbranch = await server.getBranch(this.project.id, this.target_branch);
				// console.log(this.project.name, "target_branch", tbranch);
				if (tbranch) {
					this.project.status = 'exists';
					this.parentUpdate('exists');
					this.setState({
						status: 'exists'
					});
					setImmediate(this.initNextStep);
				} else {
					this.setState({
						error: 'exists'
					});
				}
		}
	}

	render() {
		return <Card style={{ padding: 10 }}>
			<h3>{this.project.name}</h3>
			<div>
				<Stepper alternativeLabel activeStep={statuses.indexOf(this.state.status) + 1} connector={<QontoConnector />}>
					{stepperSteps.map((label) => (
						<Step key={label.title}>
							<StepLabel StepIconComponent={QontoStepIcon}>
								<span className={label.type == this.state.error ? "text-danger" : null}>
									{label.title}
								</span>
							</StepLabel>
						</Step>
					))}
				</Stepper>
			</div>
		</Card>
	}
}

class ActionTag extends Component {
	constructor(props) {
		super();
		this.id = props.match.params.id;
		let obj = localStorage.getItem(this.id);
		debugger;
		this.actionMerge = JSON.parse(obj);
	}

	updateStorage(index, status) {
		let obj = localStorage.getItem(this.id);
		obj = JSON.parse(obj);
		obj.projects[index]['status'] = status;
		localStorage.setItem(this.id, JSON.stringify(obj));
	}

	render() {
		return (
			<div>
				<Row className="m-b fixed-header">
					<Col md={4}>
						Target Branch
            <h2 className="text-info">{this.actionMerge.target_branch}</h2>
					</Col>
					<Col md={4}>
						Tag
            <h2 className="text-info">{this.actionMerge.tag_push}</h2>
					</Col>
				</Row>
				<div style={{ marginTop: 160 }}>
					{this.actionMerge.projects.map((p, index) => {
						return <Row key={p.id}>
							<Col style={{maxWidth: 1000}}>
								<TagProject 
								project={p} 
								target_branch={this.actionMerge.target_branch} 
								tag_push={this.actionMerge.tag_push}
								cancel_pipeline={this.actionMerge.cancel_pipeline}
								update={(status) => {
									this.updateStorage(index, status);
								}} />
							</Col>
						</Row>
					})}
				</div>
				<ToastContainer />
			</div>
		);
	}
}

export default ActionTag;
