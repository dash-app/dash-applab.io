import React, { Component } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Label, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Card, Input, TextField } from '@material-ui/core';

import Logo from '../../assets/images/gitlab.svg';
import monitor from '../../gitlab/monitor';
import server from '../../gitlab/server';

function getSteps() {
  return ['नामकरण..!!', 'Reachability', 'Accessibility'];
}

export default class LoginPage extends Component {

  constructor() {
    super();
    this.goAhead = this.goAhead.bind(this);
    this.goBack = this.goBack.bind(this);
    this.handleOnChange = this.handleOnChange.bind(this);
    this.authenticateUser = this.authenticateUser.bind(this);
    this.state = {
      activeStep: 0,
      project_name: "",
      gitlab_url: "",
      private_token: ""
    };
  }

  handleOnChange(event) {
    if(event.target.type == "checkbox") {
      this.setState({
        [event.target.id]: event.target.checked
      });
    } else {
      this.setState({
        [event.target.id]: event.target.value
      });
    }
  }

  goAhead() {
    document.activeElement.blur();
    this.setState(prev=> ({
      activeStep: prev.activeStep + 1
    }));
  }

  goBack() {
    document.activeElement.blur();
    this.setState(prev=> ({
      activeStep: prev.activeStep - 1
    }));
  }

  async authenticateUser() {
    let name = this.state.project_name;
    let url = this.state.gitlab_url;
    // fix the last '/' issue
    if(url.endsWith('/')) url = url.slice(0, -1);
    let private_token = this.state.private_token;
    let user = await server.getAuthenticatedUser(url, private_token);
    if(user) {
      user.private_token = private_token;
      monitor.user = user;
      monitor.gitlab.url = url;
      monitor.gitlab.name = name;
      monitor.save();
      window.location.href = '/';
    }
  }

  getStepContent() {
    switch (this.state.activeStep) {
      case 0:
        return (
          <div style={{ padding: 50, textAlign: "center", display: "flex 100%", flexDirection: "column", alignItems: "center", justifyContent: "center" }}>
            <h3 className="text-info"><b>Please enter the name of your organization or company<br /><br />
            Simply what would you like to call me..!!</b></h3>
            <Input style={{ marginTop: 10, padding:5, width: 400, textAlign: "center", fontSize: 24 }} 
            autoFocus type="text" autoComplete="on" name="project_name" id="project_name"
            onChange={this.handleOnChange} value={this.state.project_name} />
            <br />
            <Button style={{ marginTop: 30 }} variant="contained" color="primary" onClick={this.goAhead}>COOL</Button>
          </div>
        );
      case 1:
        return (
          <div style={{ padding: 50, textAlign: "center", display: "flex 100%", flexDirection: "column", alignItems: "center", justifyContent: "center" }}>
            <h3 className="text-info"><b>What is the url of your gitlab, it can surely be <span className="text-danger">https://gitlab.com</span><br /><br />
            But you might have a <span className="text-danger">self-hosted gitlab</span>, please be certain of this URL</b></h3>
            <Input placeholder="https://your-self-hosted-gitlab.domain" style={{ marginTop: 10, padding:5, width: 600, textAlign: "center", fontSize: 24 }}
             autoFocus type="text" autoComplete="on" name="gitlab_url" id="gitlab_url"
             onChange={this.handleOnChange} value={this.state.gitlab_url} />
            <br />
            <Button style={{ marginTop: 30 }} variant="contained" onClick={this.goBack}>BACK</Button>
            <Button style={{ marginTop: 30, marginLeft: 30 }} variant="contained" color="primary" onClick={this.goAhead}>NEXT</Button>
          </div>
        );
      case 2:
        return (
          <div style={{ padding: 50, textAlign: "center", display: "flex 100%", flexDirection: "column", alignItems: "center", justifyContent: "center" }}>
            <h3 className="text-info"><b>I would need access to the APIs, please enter a <span className="text-danger">private-token</span><br /><br />
            Trust me, <span className="text-danger">I'm quarantined</span> from the rest of the world.</b></h3>
            <Input placeholder="write or paste private-token" style={{ marginTop: 10, padding:5, width: 600, textAlign: "center", fontSize: 24 }}
             autoFocus type="text" autoComplete="on" name="private_token" id="private_token"
             onChange={this.handleOnChange} value={this.state.private_token} />
            <br />
            <Button style={{ marginTop: 30 }} variant="contained" onClick={this.goBack}>BACK</Button>
            <Button style={{ marginTop: 30, marginLeft: 30 }} variant="contained" color="primary" onClick={this.authenticateUser}>CONFIRM</Button>
          </div>
        );
    }
  }
  
  render() {
    return (
      <div>
        <Row><Col md={10}>
          <Card style={{ padding: 20, marginTop: 50 }}>
            <h1 style={{ textAlign: "center", margin: 20 }} className="text-orange">I'm Gitlab <img src={Logo} style={{height: 50, width: 50}}></img> Monitor</h1>
            <Stepper activeStep={this.state.activeStep}>
              {getSteps().map((label, index) => {
                const stepProps = {};
                const labelProps = {};
                return (
                  <Step key={label} {...stepProps}>
                    <StepLabel {...labelProps}>{label}</StepLabel>
                  </Step>
                );
              })}
            </Stepper>
            {this.getStepContent()}
          </Card>
        </Col> </Row>
      </div>
    );
  }
}
