import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';

import { Chip } from '@material-ui/core';

class ActionTagPage extends Component {
	constructor(props) {
		super();
		this.id = props.match.params.id;
		let actionTag = localStorage.getItem('actionTag') || '[]';
		actionTag = JSON.parse(actionTag);
		this.actionTag = [];
		actionTag.forEach(actionId => {
			try {
				let obj = JSON.parse(localStorage.getItem(actionId));
				if (obj.action === 'tag') {
					this.actionTag.push(obj);
				}
			} catch (err) {

			}
		});
	}

	render() {
		return (
			<div>
				<div className="no-fixed-content">
					{this.actionTag.length === 0 ? <h1>No Tag Actions Found</h1> : undefined}
					{this.actionTag.map((p, index) => {
						return <Row key={p.id} className="action-page-row" onClick={()=>{this.props.history.push(`/push-tag/${p.id}`);}}>
								<Col md={4}>
									Target Branch <h2 className="text-info">{p.target_branch}</h2>
									Tag <h2 className="text-info">{p.tag_push}</h2>
								</Col>
								<Col md={8}>
									<h4 className="text-info">Project(s)</h4>
									{p.projects.map(p => {
										return <Chip key={p.id} label={p.name} style={{ fontSize: 16, marginRight: 10 }}
										variant="outlined" color="primary" />
									})}
								</Col>
						</Row>
					})}
				</div>
			</div>
		);
	}
}

export default ActionTagPage;
