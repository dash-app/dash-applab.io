import React, { Component } from 'react';
import { Row, Col, Card } from 'reactstrap';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
// import Stepper from 'react-stepper-horizontal';

import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import { StepConnector } from '@material-ui/core';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Check from '@material-ui/icons/Check';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { Loader } from '../../vibe';

import server from '../../gitlab/server';

let statuses = ["exists", "create", "merge", "cancel"];
let stepperSteps = [
	{ title: 'Branches Exists', type: 'exists' },
	{ title: 'Create MR', type: 'create' },
	{ title: 'Merge MR', type: 'merge' },
	{ title: 'Cancel Pipeline', type: 'cancel' }
];

const QontoConnector = withStyles({
	alternativeLabel: {
		top: 10,
		left: 'calc(-50% + 16px)',
		right: 'calc(50% + 16px)',
	},
	active: {
		'& $line': {
			borderColor: '#0897ee',
		},
	},
	completed: {
		'& $line': {
			borderColor: '#0897ee',
		},
	},
	line: {
		borderColor: '#eaeaf0',
		borderTopWidth: 3,
		borderRadius: 1,
	},
})(StepConnector);

const useQontoStepIconStyles = makeStyles({
	root: {
		color: '#eaeaf0',
		display: 'flex',
		height: 22,
		alignItems: 'center',
	},
	active: {
		color: '#04ab3a',
	},
	circle: {
		width: 8,
		height: 8,
		borderRadius: '50%',
		backgroundColor: 'currentColor',
	},
	completed: {
		color: '#04ab3a',
		zIndex: 1,
		fontSize: 30
	},
});

function QontoStepIcon(props) {
	const classes = useQontoStepIconStyles();
	const { active, completed } = props;

	return (
		<div
			className={clsx(classes.root, {
				[classes.active]: active,
			})}
		>
			{/* {completed ? <Check className={classes.completed} /> : <div className={classes.circle} />} */}
			{completed ? <Check className={classes.completed} /> :
				active ? <div><Loader type="spin" small /></div> : <div className={classes.circle} />}
		</div>
	);
}

QontoStepIcon.propTypes = {
	/**
	 * Whether this step is active.
	 */
	active: PropTypes.bool,
	/**
	 * Mark the step as completed. Is passed to child components.
	 */
	completed: PropTypes.bool,
};

class MergeProject extends Component {
	constructor(props) {
		super();
		this.project = props.project;
		this.source_branch = props.source_branch;
		this.target_branch = props.target_branch;
		this.cancel_pipeline = props.cancel_pipeline;
		this.parentUpdate = props.update;
		this.initNextStep = this.initNextStep.bind(this);
		this.state = {
			status: this.project.status,
			error: this.project.error
		}
	}

	componentDidMount() {
		setImmediate(this.initNextStep);
	}

	async initNextStep() {
		switch (this.project.status) {
			case 'exists':
				// create MR
				let mr = await server.createMergeRequest(this.project.id, this.source_branch, this.target_branch);
				if (mr) {
					console.log(this.project.name, mr);
					this.project.mr = mr;
					this.project.status = 'create';
					this.parentUpdate(this.project);
					this.setState({
						status: 'create'
					});
					setImmediate(this.initNextStep);
				} else {
					this.setState({
						error: 'create'
					});
				}
				break;
			case 'create':
				// merge MR
				let mergedMR = await server.mergeMR(this.project.id, this.project.mr.iid);
				console.log(this.project.name, mergedMR);
				if (mergedMR) {
					console.log(this.project.name, mergedMR);
					this.project.mr = mergedMR;
					this.cancel_pipeline = this.project.cancel_pipeline && mergedMR.state == 'merged';
					this.project.status = 'merge';
					this.parentUpdate(this.project);
					this.setState({
						status: 'merge'
					});
					setImmediate(this.initNextStep);
				} else {
					this.setState({
						error: 'merge'
					});
				}
				break;
			case 'merge':
				// merge_commit_sha cancel pipeline
				if(this.cancel_pipeline) {
					let res = await server.cancelPipelines(this.project.id, this.project.mr.merge_commit_sha);
					if(!res) {
						this.setState({
							error: 'cancel'
						});
					}
				}
				// success
				this.project.status = 'cancel';
				this.parentUpdate(this.project);
				this.setState({
					status: 'cancel'
				});
				break;
			case 'cancel':
				break;
			default:
				// check branches
				let sbranch = await server.getBranch(this.project.id, this.source_branch);
				// console.log(this.project.name, "source_branch", sbranch);
				let tbranch = await server.getBranch(this.project.id, this.target_branch);
				// console.log(this.project.name, "target_branch", tbranch);
				if (sbranch && tbranch) {
					this.project.status = 'exists';
					this.parentUpdate(this.project);
					this.setState({
						status: 'exists'
					});
					setImmediate(this.initNextStep);
				} else {
					this.setState({
						error: 'exists'
					});
				}
		}
	}

	render() {
		return <Card style={{ padding: 10 }}>
			<h3>{this.project.name}</h3>
			<div>
				<Stepper alternativeLabel activeStep={statuses.indexOf(this.state.status) + 1} connector={<QontoConnector />}>
					{stepperSteps.map((label) => (
						<Step key={label.title}>
							<StepLabel StepIconComponent={QontoStepIcon}>
								<span className={label.type == this.state.error ? "text-danger" : null}>
									{label.title}
								</span>
							</StepLabel>
						</Step>
					))}
				</Stepper>
			</div>
		</Card>
	}
}

class ActionMerge extends Component {
	constructor(props) {
		super();
		this.id = props.match.params.id;
		let obj = localStorage.getItem(this.id);
		this.actionMerge = JSON.parse(obj);
	}

	updateStorage(index, project) {
		let obj = localStorage.getItem(this.id);
		obj = JSON.parse(obj);
		obj.projects[index] = project;
		localStorage.setItem(this.id, JSON.stringify(obj));
	}

	render() {
		return (
			<div>
				<Row className="m-b" className="fixed-header">
					<Col md={4}>
						Source Branch
            <h2 className="text-info">{this.actionMerge.source_branch}</h2>
					</Col>
					<Col md={4}>
						Target Branch
            <h2 className="text-info">{this.actionMerge.target_branch}</h2>
					</Col>
				</Row>
				<div style={{ marginTop: 160 }}>
					{this.actionMerge.projects.map((p, index) => {
						return <Row key={p.id}>
							<Col style={{maxWidth: 1000}}>
								<MergeProject 
								project={p} 
								target_branch={this.actionMerge.target_branch} 
								source_branch={this.actionMerge.source_branch}
								update={(project) => {
									this.updateStorage(index, project);
								}} />
							</Col>
						</Row>
					})}
				</div>
				<ToastContainer />
			</div>
		);
	}
}

export default ActionMerge;
