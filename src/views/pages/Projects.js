import React, { Component } from 'react';
import { Label, Input, Row, Col, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import uuid from 'uuid/v4';

import ProjectCard from '../elements/ProjectCard';
import SelectBranchComponent from '../elements/SelectBranchComponent';
import SelectTagsComponent from '../elements/SelectTagsComponent';
import server from '../../gitlab/server';
import monitor from '../../gitlab/monitor';
import { Loader } from '../../vibe';
import { Chip } from '@material-ui/core';

class Projects extends Component {
  constructor() {
    super();
    this.togglMergeModal = this.togglMergeModal.bind(this);
    this.togglCreateModal = this.togglCreateModal.bind(this);
    this.toggleTagModal = this.toggleTagModal.bind(this);
    this.handleOnChange = this.handleOnChange.bind(this);
    this.saveActionMerge = this.saveActionMerge.bind(this);
    this.saveActionCreate = this.saveActionCreate.bind(this);
    this.saveActionTag = this.saveActionTag.bind(this);
    this.refreshProjects = this.refreshProjects.bind(this);
    this.updateProject = this.updateProject.bind(this);
    this.getFilteredProjects = this.getFilteredProjects.bind(this);
    this.togglDropDown = this.togglDropDown.bind(this);
    this.selectAllFiltered = this.selectAllFiltered.bind(this);
    this.clearAll = this.clearAll.bind(this);
    this.state = {
      mergeModal: false,
      createModal: false,
      tagModal: false,
      loading: true,
      target_branch: "",
      source_branch: "",
      tag_push: "",
      cancel_pipeline: false,
      dropdown: false,
      search: "",
      projects: []
    }
  }

  componentDidMount() {
    setImmediate(this.refreshProjects);
  }

  getFilteredProjects() {
    return this.state.projects.filter(p => {
      return this.state.search.length == 0 || p.web_url.toLowerCase().indexOf(this.state.search.toLowerCase()) >= 0;
    });
  }

  updateProject(project) {
    let ps = this.state.projects;
    ps.forEach((p, i) => {
      ps[i] = p.id===project.id ? project : p;
    });
    this.setState({
      projects: ps
    });
  }

  async refreshProjects() {
    this.setLoading(true);
    let projects = await server.getProjects();
    this.setState({
      projects: projects,
      loading: false
    });
  }

  setLoading(isLoading) {
    this.setState({
      loading: isLoading
    });
  }

  handleOnChange(event) {
    if (event.target.type == "checkbox") {
      this.setState({
        [event.target.id]: event.target.checked
      });
    } else {
      this.setState({
        [event.target.id]: event.target.value
      });
    }
  }

  togglMergeModal() {
    if (!this.state.mergeModal) {
      // if we have projects
      let projects = this.state.projects.filter(p => p.selected);
      if (projects.length == 0) {
        toast("Please select atleast 1 project !", {
          type: "error"
        });
        return;
      }
    }
    this.setState(prevState => ({
      mergeModal: !prevState.mergeModal
    }));
  }

  toggleTagModal() {
    if (!this.state.tagModal) {
      // if we have projects
      let projects = this.state.projects.filter(p => p.selected);
      if (projects.length == 0) {
        toast("Please select atleast 1 project !", {
          type: "error"
        });
        return;
      }
    }
    this.setState(prevState => ({
      tagModal: !prevState.tagModal
    }));
  }

  togglDropDown() {
    this.setState(prevState => ({
      dropdown: !prevState.dropdown
    }));
  }

  saveActionMerge() {
    let projects = this.state.projects.filter(p => p.selected);
    let obj = {
      action: 'merge',
      time: new Date().getTime(),
      source_branch: this.state.source_branch,
      target_branch: this.state.target_branch,
      cancel_pipeline: this.state.cancel_pipeline,
      id: uuid(),
      projects: projects
    };
    monitor.addEntry("actionMerge", obj);
    this.props.history.push(`/merging/${obj.id}`);
  }

  togglCreateModal() {
    if (!this.state.createModal) {
      // if we have projects
      let projects = this.state.projects.filter(p => p.selected);
      if (projects.length == 0) {
        toast("Please select atleast 1 project !", {
          type: "error"
        });
        return;
      }
    }
    this.setState(prevState => ({
      createModal: !prevState.createModal
    }));
  }

  saveActionCreate() {
    let projects = this.state.projects.filter(p => p.selected);
    let obj = {
      action: 'create',
      time: new Date().getTime(),
      source_branch: this.state.source_branch,
      target_branch: this.state.target_branch,
      cancel_pipeline: this.state.cancel_pipeline,
      id: uuid(),
      projects: projects
    };
    monitor.addEntry("actionCreate", obj);
    this.props.history.push(`/create-branch/${obj.id}`);
  }

  saveActionTag() {
    let projects = this.state.projects.filter(p => p.selected);
    let obj = {
      action: 'tag',
      time: new Date().getTime(),
      tag_push: this.state.tag_push,
      target_branch: this.state.target_branch,
      cancel_pipeline: this.state.cancel_pipeline,
      id: uuid(),
      projects: projects
    };
    monitor.addEntry("actionTag", obj);
    this.props.history.push(`/push-tag/${obj.id}`);
  }

  selectAllFiltered(event) {
    let projects = this.state.projects;
    this.state.projects.forEach(p => {
      if (this.state.search.length === 0 || p.web_url.toLowerCase().indexOf(this.state.search.toLowerCase()) >= 0) {
        p.selected = true;
      }
    });
    this.setState({
      projects: projects
    });
    event.target.blur();
  }

  clearAll(event) {
    let projects = this.state.projects;
    this.state.projects.forEach(p => {
        p.selected = false;
    });
    this.setState({
      projects: projects
    });
    event.target.blur();
  }

  render() {
    let selectedProjects = this.state.projects.filter(p => {
      return p.selected;
    });
    let filteredProjects = this.getFilteredProjects();
    return (
      <div>
        {this.getMergeModal()}
        {this.getCreateModal()}
        {this.getTagModal()}
        {/* <Label for="name">Search for actions</Label> */}
        {/* <Row style={{position:'fixed', padding: 20, background: '#ffffff', zIndex: 100, width: '100%'}}> */}
        <Row className="fixed-header">
          <Col md={4}>
            <Input type="text" autoComplete="off" onChange={this.handleOnChange} name="search" id="search" placeholder="Project Name or Group" />
          </Col>
          <Col md={8}>
            {this.getDropDown()}
            {/* <Button color="info" onClick={this.togglCreateModal}>Create New Branch</Button> */}
            {/* <Button style={{ marginLeft: 20 }} color="info" onClick={this.togglMergeModal}>Merge Branches</Button> */}
          </Col>
          <Col md={2}>
          </Col>
        </Row>
        {/* <Row style={{ marginTop: 20 }}>
        </Row> */}
        {/* <Row style={{ marginTop: 70, background:'#f2f4f7' }}> */}
        <Row className="fixed-content">
          <Col md={12} style={{ margin: 10 }}>
            <Button color="success" disabled={filteredProjects.length === 0} onClick={this.selectAllFiltered}>Select All {filteredProjects.length ? `(${filteredProjects.length})` : ''}</Button>
            {' '}
            <Button color="warning" disabled={selectedProjects.length === 0} onClick={this.clearAll}>Clear All {selectedProjects.length ? `(${selectedProjects.length})` : ''}</Button>{' '}
            {selectedProjects.map(p => {
              return <Chip key={p.id} label={p.name} style={{ fontSize: 16, marginRight: 10 }} variant="outlined"
               onDelete={() => {p.selected=false; this.updateProject(p)}} color="primary" />
            })}
          </Col>
          {this.state.loading ?
            <Col style={{ marginTop: 100 }}>
              <span><Loader type="bars" /></span>
            </Col> :
            filteredProjects.map((p) => {
              return <ProjectCard key={p.id} project={p} parentUpdate={this.updateProject} />
            })
          }
        </Row>
        <ToastContainer />
      </div>
    );
  }

  getDropDown() {
    return <Dropdown isOpen={this.state.dropdown} toggle={this.togglDropDown}>
      <DropdownToggle caret>
        Actions
      </DropdownToggle>
      <DropdownMenu>
        {/* <DropdownItem header>Header</DropdownItem> */}
        <DropdownItem onClick={this.togglCreateModal}>Create New Branch</DropdownItem>
        {/* <DropdownItem disabled>Action (disabled)</DropdownItem> */}
        {/* <DropdownItem divider /> */}
        <DropdownItem onClick={this.togglMergeModal}>Merge Branches</DropdownItem>
        <DropdownItem onClick={this.toggleTagModal}>Push Tag</DropdownItem>
        {/* <DropdownItem>Bar Action</DropdownItem> */}
        {/* <DropdownItem>Quo Action</DropdownItem> */}
      </DropdownMenu>
    </Dropdown>;
  }

  getMergeModal() {
    let project = this.state.projects.find(p => p.selected);
    let projectId = (project && project.id) ? project.id : 1;
    return <Modal isOpen={this.state.mergeModal} toggle={this.togglMergeModal}>
      <ModalHeader toggle={this.togglMergeModal}>
        Select the branches to be merged
      </ModalHeader>
      <ModalBody>
        <Row className="m-b">
          <Col>
            <strong>Source Branch</strong>
            <SelectBranchComponent style={{ marginTop: 10 }} projectId={projectId} id="source_branch"  onChange={(value) => {this.setState({"source_branch" : value})}}/>
          </Col>
          <Col>
            <strong>Target Branch</strong>
            <SelectBranchComponent style={{ marginTop: 10 }} projectId={projectId} id="target_branch" onChange={(value) => {this.setState({"target_branch" : value})}}/>
          </Col>
        </Row>
        <Row>
          <Col>
            <Label check>
              <Input checked={this.state.cancel_pipeline} onChange={this.handleOnChange} id="cancel_pipeline" type="checkbox" />{' '}
            Cancel any pipeline initiated
          </Label>
          </Col>
        </Row>
      </ModalBody>
      <ModalFooter>
        {/* save the actionmerge object to local storage */}
        <Button color="primary" onClick={this.saveActionMerge}>Start Merge Process&nbsp;&nbsp;<i className="fa fa-arrow-right"></i></Button>{' '}
      </ModalFooter>
    </Modal>;
  }

  getCreateModal() {
    let project = this.state.projects.find(p => p.selected);
    let projectId = (project && project.id) ? project.id : 1;
    return <Modal isOpen={this.state.createModal} toggle={this.togglCreateModal}>
      <ModalHeader toggle={this.togglCreateModal}>
        Select the branches
      </ModalHeader>
      <ModalBody>
        <Row className="m-b">
          <Col>
            <strong>Source Branch</strong>
            <SelectBranchComponent style={{ marginTop: 10 }} projectId={projectId} id="source_branch"  onChange={(value) => {this.setState({"source_branch" : value})}}/>
          </Col>
          <Col>
            <strong>Branch to create</strong>
            <SelectBranchComponent style={{ marginTop: 10 }} projectId={projectId} id="target_branch" newText="New:"  onChange={(value) => {this.setState({"target_branch" : value})}}/>
          </Col>
        </Row>
        <Row>
          <Col>
            <Label check>
              <Input checked={this.state.cancel_pipeline} onChange={this.handleOnChange} id="cancel_pipeline" type="checkbox" />{' '}
            Cancel any pipeline initiated
          </Label>
          </Col>
        </Row>
      </ModalBody>
      <ModalFooter>
        {/* save the actionmerge object to local storage */}
        <Button color="primary" onClick={this.saveActionCreate}>Create Branch&nbsp;&nbsp;<i className="fa fa-arrow-right"></i></Button>{' '}
      </ModalFooter>
    </Modal>;
  }

  getTagModal() {
    let project = this.state.projects.find(p => p.selected);
    let projectId = (project && project.id) ? project.id : 1;
    return <Modal isOpen={this.state.tagModal} toggle={this.toggleTagModal}>
      <ModalHeader toggle={this.toggleTagModal}>
        Fill In The Blanks :) 
      </ModalHeader>
      <ModalBody>
        <Row className="m-b">
          <Col>
            <strong>Target Branch</strong>
            <SelectBranchComponent style={{ marginTop: 10 }} projectId={projectId} id="target_branch"  onChange={(value) => {this.setState({"target_branch" : value})}}/>
          </Col>
          <Col>
            <strong>Tag to push</strong>
            <SelectTagsComponent style={{ marginTop: 10 }} projectId={projectId} id="tag_push"  onChange={(value) => {this.setState({"tag_push" : value})}}/>
          </Col>
        </Row>
        <Row>
          <Col>
            <Label check>
              <Input checked={this.state.cancel_pipeline} onChange={this.handleOnChange} id="cancel_pipeline" type="checkbox" />{' '}
            Cancel any pipeline initiated
          </Label>
          </Col>
        </Row>
      </ModalBody>
      <ModalFooter>
        {/* save the actionmerge object to local storage */}
        <Button color="primary" onClick={this.saveActionTag}>Push tag&nbsp;&nbsp;<i className="fa fa-arrow-right"></i></Button>{' '}
      </ModalFooter>
    </Modal>;
  }
}

export default Projects;
