class Monitor {

    constructor() {
        this.gitlab = this.get('gitlab') || {
            name: '',
            url: ''
        };
        this.application = this.get('application') || {
            clientId: '',
            clientSecret: ''
        };
        this.user = this.get('user');
    }

    getGitlabConfig() {
        return this.gitlab;
    }

    getApplication() {
        return this.application;
    }

    getUser() {
        return this.user;
    }

    logout() {
        localStorage.removeItem('gitlab');
        localStorage.removeItem('application');
        localStorage.removeItem('user');
    }

    isLoggedIn() {
        return this.user.private_token.length > 0 ||
            this.user.auth_code.length > 0;
    }

    isValidApplication() {
        return this.application.clientSecret.length > 0 &&
            this.application.clientId.length > 0;
    }

    get(key) {
        try {
            return JSON.parse(localStorage.getItem(key));
        } catch (error) {
            return undefined;
        }
    }

    addEntry(key, entry) {
        try {
            let arr = this.get(key) || [];
            arr.push(entry.id);
            localStorage.setItem(entry.id, JSON.stringify(entry));
            localStorage.setItem(key, JSON.stringify(arr));
        } catch (error) {
            
        }
    }

    save() {
        localStorage.setItem("gitlab", JSON.stringify(this.gitlab));
        localStorage.setItem("application", JSON.stringify(this.application));
        localStorage.setItem("user", JSON.stringify(this.user));
    }

}

export default new Monitor();
