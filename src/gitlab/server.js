
import monitor from './monitor';
import fetchWithCache from './fetchWithCache';

class Server {

    constructor() {
        this.gitlab = monitor.getGitlabConfig();
        console.log(this.gitlab);
        this.projects = [];
    }

    getApiOpts() {
        let headers = {
            'Private-Token': monitor.getUser().private_token
        };
        return {
            headers: headers
        };
    }

    getAuthenticatedUser(url, token) {
        let headers = new Headers({ 'Private-Token': token });
        url = `${url}/api/v4/user`;
        return fetch(url, {
            headers: headers
        }).then(res => res.json());
    }

    checkIfLoggedIn() {
        if(!monitor.getUser()) {
            window.location.href = process.env.PUBLIC_URL + "/login";
        }
    }

    getProjectBranches(projectId, cache=true) {
        this.checkIfLoggedIn();
        let url = `${this.gitlab.url}/api/v4/projects/${projectId}/repository/branches`;
        return fetchWithCache.fetch(url, this.getApiOpts(), cache);//.then(res=>res.json());
    }

    async getProjects() {
        this.checkIfLoggedIn();
        if (this.projects.length > 0) {
            return this.projects;
        }
        // add pagination
        let res = [];
        let page = 1;
        let projects = [];
        do {
            let url = `${this.gitlab.url}/api/v4/projects?page=${page}&per_page=10&membership=true`;
            projects = await fetchWithCache.fetch(url, this.getApiOpts());//.then(res => res.json());
            res = res.concat(projects);
            page++;
        } while (projects.length > 0);
        this.projects = res;
        return this.projects;
    }

    getUsers(page) {
        this.checkIfLoggedIn();
        let url = `${this.gitlab.url}/api/v4/users?page=${page}&per_page=50`;
        return fetch(url, this.getApiOpts());
    }

    getProjectTags(projectId, cache=true) {
        this.checkIfLoggedIn();
        let url = `${this.gitlab.url}/api/v4/projects/${projectId}/repository/tags`;
        return fetchWithCache.fetch(url, this.getApiOpts(), cache);//.then(res=>res.json());
    }

    getMergeRequests(projectId) {
        this.checkIfLoggedIn();
        let url = `${this.gitlab.url}/api/v4/projects/${projectId}/merge_requests`;
        return fetch(url, this.getApiOpts());
    }

    getCommits(projectId) {
        this.checkIfLoggedIn();
        let url = `${this.gitlab.url}/api/v4/projects/${projectId}/commits`;
        return fetch(url, this.getApiOpts());
    }

    getDiscussions(projectId, mrId) {
        this.checkIfLoggedIn();
        let url = `${this.gitlab.url}/api/v4/projects/${projectId}/merge_requests/${mrId}/discussions`;
        return fetch(url, this.getApiOpts());
    }

    async getBranch(projectId, branchName) {
        this.checkIfLoggedIn();
        let url = `${this.gitlab.url}/api/v4/projects/${projectId}/repository/branches/${branchName}`;
        try {
            let branch = await fetch(url, this.getApiOpts()).then(res => res.json());
            if (branch && branch.name) {
                return branch;
            }
        } catch (error) {
            console.error(error);
        }
        return null;
    }

    async createMergeRequest(projectId, source_branch, target_branch, title) {
        this.checkIfLoggedIn();
        title = title || `Merging ${source_branch} to ${target_branch}`;
        let url = `${this.gitlab.url}/api/v4/projects/${projectId}/merge_requests?source_branch=${source_branch}&target_branch=${target_branch}&title=${title}`;
        try {
            let mr = await fetch(url, { ...this.getApiOpts(), method: 'POST' }).then(res => res.json());
            if (mr && mr.iid) {
                return mr;
            } else {
                mr = await this.getMergeRequest(projectId, source_branch, target_branch, 'opened');
                if (mr && mr.length > 0) {
                    return mr[0];
                }
            }
        } catch (error) {
            console.error(error);
        }
        return null;
    }

    async getMergeRequest(projectId, source_branch, target_branch, state) {
        this.checkIfLoggedIn();
        let url = `${this.gitlab.url}/api/v4/projects/${projectId}/merge_requests?source_branch=${source_branch}&target_branch=${target_branch}&state=${state}`;
        try {
            let mr = await fetch(url, { ...this.getApiOpts(), method: 'GET' }).then(res => res.json());
            if (mr && mr.length > 0) {
                return mr;
            }
        } catch (error) {
            console.error(error);
        }
        return null;
    }

    async pushtag(projectId, ref, tag) {
        this.checkIfLoggedIn();
        let url = `${this.gitlab.url}/api/v4/projects/${projectId}/repository/tags?tag_name=${tag}&ref=${ref}`;
        try {
            let tag = await fetch(url, { ...this.getApiOpts(), method: 'POST' }).then(res => res.json());
            return tag;
        } catch (error) {
            console.error(error);
        }
        return null;
    }

    async mergeMR(projectId, mrId) {
        this.checkIfLoggedIn();
        let url = `${this.gitlab.url}/api/v4/projects/${projectId}/merge_requests/${mrId}/merge`;
        try {
            let mr = await fetch(url, { ...this.getApiOpts(), method: 'PUT' }).then(res => res.json());
            // if MR is work in progress
            if (!mr.iid) {
                mr = await this.getMrWithChanges(projectId, mrId);
                if (mr && mr.changes && mr.changes.length == 0) {
                    return await this.closeMR(projectId, mrId);
                }
            }
            return mr;
        } catch (error) {
            console.error(error);
        }
        return null;
    }

    async getMrWithChanges(projectId, mrId) {
        this.checkIfLoggedIn();
        let url = `${this.gitlab.url}/api/v4/projects/${projectId}/merge_requests/${mrId}/changes`;
        try {
            let mr = await fetch(url, { ...this.getApiOpts(), method: 'GET' }).then(res => res.json());
            return mr;
        } catch (error) {
            console.error(error);
        }
        return null;
    }

    async closeMR(projectId, mrId) {
        this.checkIfLoggedIn();
        let url = `${this.gitlab.url}/api/v4/projects/${projectId}/merge_requests/${mrId}?state_event=close`;
        try {
            let mr = await fetch(url, { ...this.getApiOpts(), method: 'PUT' }).then(res => res.json());
            return mr;
        } catch (error) {
            console.error(error);
        }
        return null;
    }

    async cancelPipelinesBySha(projectId, sha) {
        this.checkIfLoggedIn();
        return await this.cancelPipelines(projectId, 'sha', sha);
    }

    async cancelPipelinesByRef(projectId, ref) {
        this.checkIfLoggedIn();
        return await this.cancelPipelines(projectId, 'ref', ref);
    }

    async cancelPipelines(projectId, type, value) {
        this.checkIfLoggedIn();
        let res = [];
        try {
            let pipelines = await this.getCreatedPipelines(projectId, type, value);
            for (let index = 0; index < pipelines.length; index++) {
                const element = pipelines[index];
                await this.cancelPipeline(projectId, element.id);
                res.push(element.web_url);
            }
            pipelines = await this.getPendingPipelines(projectId, type, value);
            for (let index = 0; index < pipelines.length; index++) {
                const element = pipelines[index];
                await this.cancelPipeline(projectId, element.id);
                res.push(element.web_url);
            }
            pipelines = await this.getRunningPipelines(projectId, type, value);
            for (let index = 0; index < pipelines.length; index++) {
                const element = pipelines[index];
                await this.cancelPipeline(projectId, element.id);
                res.push(element.web_url);
            }
            return res;
        } catch (error) {
            console.log(error);
        }
        return false;
    }

    async getRunningPipelines(projectId, type, value) {
        this.checkIfLoggedIn();
        return await this.getPipelines(projectId, type, value, 'running');
    }

    async getPendingPipelines(projectId, type, value) {
        this.checkIfLoggedIn();
        return await this.getPipelines(projectId, type, value, 'pending');
    }

    async getCreatedPipelines(projectId, type, value) {
        this.checkIfLoggedIn();
        return await this.getPipelines(projectId, type, value, 'created');
    }

    async getPipelines(projectId, type, value, status) {
        this.checkIfLoggedIn();
        let url = `${this.gitlab.url}/api/v4/projects/${projectId}/pipelines?${type}=${value}&status=${status}`;
        try {
            let pipelines = await fetch(url, { ...this.getApiOpts(), method: 'GET' }).then(res => res.json());
            return pipelines;
        } catch (error) {
            console.error(error);
        }
        return null;
    }


    async cancelPipeline(projectId, pipelineId) {
        this.checkIfLoggedIn();
        let url = `${this.gitlab.url}/api/v4/projects/${projectId}/pipelines/${pipelineId}/cancel`;
        try {
            let pipelines = await fetch(url, { ...this.getApiOpts(), method: 'POST' }).then(res => res.json());
            return pipelines;
        } catch (error) {
            console.error(error);
        }
        return null;
    }

    async createBranch(projectId, source_branch, target_branch) {
        this.checkIfLoggedIn();
        let url = `${this.gitlab.url}/api/v4/projects/${projectId}/repository/branches?ref=${source_branch}&branch=${target_branch}`;
        try {
            let branch = await fetch(url, { ...this.getApiOpts(), method: 'POST' }).then(res => res.json());
            return branch;
        } catch (error) {
            console.error(error);
        }
        return null;
    }
}

export default new Server();