var crypto = require('crypto');

class FetchWithCache {

    constructor() {
        this.cache = {};
    }

    async waitAndReturn(value, timeout) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(value);
            }, timeout);
        });
    }

    async fetch(url, options, cache=true) {
        let key = crypto.createHash('md5').update(url).digest("hex");
        let value = cache? this.cache[key] : undefined;
        value = value? await this.waitAndReturn(value, 0) : await fetch(url, options).then(res=>res.json());
        this.cache[key] = value;
        return value;
    }
}

export default new FetchWithCache();
