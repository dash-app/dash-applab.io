import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Button, Badge, NavItem, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { Header, SidebarNav, Footer, PageContent, Avatar, Chat, PageAlert, Page } from '../vibe';
// import Logo from '../assets/images/vibe-logo.svg';
import Logo from '../assets/images/gitlab.svg';
import avatar1 from '../assets/images/avatar1.png';
import nav from '../_nav';
import routes from '../views';
import ContextProviders from '../vibe/components/utilities/ContextProviders';
import handleKeyAccessibility, { handleClickAccessibility } from '../vibe/helpers/handleTabAccessibility';

import monitor from '../gitlab/monitor';

const MOBILE_SIZE = 992;

export default class DashboardLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sidebarCollapsed: false,
      isMobile: window.innerWidth <= MOBILE_SIZE,
      showChat1: true,
    };
    this.user = monitor.getUser();
  }

  handleResize = () => {
    if (window.innerWidth <= MOBILE_SIZE) {
      this.setState({ sidebarCollapsed: false, isMobile: true });
    } else {
      this.setState({ isMobile: false });
    }
  };

  componentDidUpdate(prev) {
    if (this.state.isMobile && prev.location.pathname !== this.props.location.pathname) {
      this.toggleSideCollapse();
    }
  }

  componentDidMount() {
    window.addEventListener('resize', this.handleResize);
    document.addEventListener('keydown', handleKeyAccessibility);
    document.addEventListener('click', handleClickAccessibility);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }

  toggleSideCollapse = () => {
    this.setState(prevState => ({ sidebarCollapsed: !prevState.sidebarCollapsed }));
  };

  closeChat = () => {
    this.setState({ showChat1: false });
  };

  render() {
    const { sidebarCollapsed } = this.state;
    const sidebarCollapsedClass = sidebarCollapsed ? 'side-menu-collapsed' : '';
    return (
      <ContextProviders>
        <div className={`app ${sidebarCollapsedClass}`}>
          <PageAlert />
          {this.getPage()}
          <Footer>
            {/* <span>Copyright © 2020 Rajdeep Deb. All rights reserved.</span> */}
            {/* <span>
              <a href="#!">Terms</a> | <a href="#!">Privacy Policy</a>
            </span> */}
            {/* <span className="ml-auto hidden-xs">
              Made with{' '}
              <span role="img" aria-label="taco">
                🌮
              </span>
            </span> */}
          </Footer>
          {/* <Chat.Container>
            {this.state.showChat1 && (
              <Chat.ChatBox name="Messages" status="online" image={avatar1} close={this.closeChat} />
            )}
          </Chat.Container> */}
        </div>
      </ContextProviders>
    );
  }

  getPage() {
    const { sidebarCollapsed } = this.state;
    const sidebarCollapsedClass = sidebarCollapsed ? 'side-menu-collapsed' : '';
    return (
      <div className="app-body">
        {
          this.user ?
            <SidebarNav
              nav={nav}
              logo={Logo}
              logoText="Gitlab Monitor"
              isSidebarCollapsed={sidebarCollapsed}
              toggleSidebar={this.toggleSideCollapse}
              {...this.props}
            /> : null}
        <Page>
          {this.user ?
            <Header
              toggleSidebar={this.toggleSideCollapse}
              isSidebarCollapsed={sidebarCollapsed}
              routes={routes}
              {...this.props}
            >
              <HeaderNav user={this.user} />
            </Header>
            : null}
          <PageContent>
            <Switch>
              {routes.map((page, key) => (
                <Route path={page.path} component={page.component} key={key} />
              ))}
              <Redirect from="/" to="/projects" />
            </Switch>
          </PageContent>
        </Page>
      </div>
    );
  }
}

function HeaderNav(props) {
  return (
    <React.Fragment>
      <UncontrolledDropdown nav inNavbar>
        <DropdownToggle nav>
          {props.user.name} {' '}
          <Avatar image={props.user.avatar_url} />
        </DropdownToggle>
        <DropdownMenu right>
          <DropdownItem onClick={() => {
            window.open(props.user.web_url, '_blank');
          }}>Profile</DropdownItem>
          <DropdownItem divider />
          <DropdownItem onClick={() => {
            monitor.logout();
            window.location = '/';
          }}>Logout</DropdownItem>
        </DropdownMenu>
      </UncontrolledDropdown>
    </React.Fragment>
  );
}
